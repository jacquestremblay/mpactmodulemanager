# MPact module manager


## Communication with the MPact Module
The module communicate via the USB port through a virtual serial port  
You can use any terminal application that support RS232 protocol  
The speed is 19200 bauds, 8 bits, no parity and 1 stop bit  

**Windows**   
In Windows you have to install FTDI drivers: 
https://www.ftdichip.com/FTDrivers.htm    
You can use terminal application like putty: https://putty.org/   

**Linux**   
In Linux, drivers are built in and you should see device named /dev/ttyUSB0

Once connected you can interact with the module with several commands

You can also use the program written in python to communicate with the module.  
It is a text base application and on the command line you type   

`sudo ./mpactModule.py`   

You will access a menu with 4 choices:   

**1 - Initialisation**       
Send default configuration to the MPact module   
**2 - Display MPact module config**   
Will display configuration in a friendly user format   
**3 - Send command**   
You can send a command to the module to change the config then display config (choice 2 ) to see your
changes.     
**4 - Listen to MPact module**   
This fonction let you listen the MPact module and test de motion sensor detector. When a motion is
detected the MPact module send to the console MD


## Repository
You can find updates for the manager and documentation on gitlab.   
git clone https://gitlab.com/jacquestremblay/mpactmodulemanager.git   
The repository is public but read only


## Commands
A valid command is a 2 alphanumeric characters and the letters must be in upper case   
There are 2 types of commands,   



#### 1 -  Commands that return informations:   
These commands must be followed by a carriage return (enter). If the command is
valid the module will reply with a caracters string. If not the module will reply nothing.  

**VR** Return the microcode version of the MPact module   

**GS** Return system state Gnnlpaccccttffrrwksngn   
nn = not used   
l = LCD alimentation (0=OFF, 1=ON)   
p = PC alimentation (0=OFF, 1=ON)   
a = Audio amplifier (0=OFF, 1=ON)   
cccc = Electric current consumption in milliamps (0000 to 9999)   
tt = tempererature in celcius (00 to 99)    
ff = Left fan speed in RPM/100 (00 to 99)    
rr = Right fan speed in RPM/100 (00 to 99)    
w = Watchdog setting (0 to 9)    
k = LCD activation setting (0 to 9)    
s = Audio activation setting (0 to 9)    
n = not used    
g = Fan speed setting (0 to 9)   
n = not used    

**Exemple**        
String received: G001102040284500362040   
00 = not used    
1 = LCD DC output = ON   
1 = PC DC output = ON   
0 = Audio amplifier OFF
2040 = Current is 2040 milliamps    
28 = Temperature is 28 celcius
45 = Left fan speed is 45x100 = 4500 RPM   
00 = Right fan speed is 0 (the fan doesn't work)   
3 = Watchdog set to 3 x 5 min = 15 minutes (see notes below)    
6 = LCD activation set to 6 x 10 min = 60 minutes (see notes below)   
2 = Audio activation set to 2 x 1 min = 2 minutes (see notes below)   
0 = not used   
4 = Fans speed set to 4   
0 = not used   

**Notes on watchdog**  
The watchdog is a feature that will perform a power cycle on the PC DC output if watchdog 
reach his threshold. In the previous example the watchdog is set to 15 minutes. As soon 
as the PC perform a command on the MPact Module through the serial port, the watchdog counter
is reset to 0. If it reach the 15 min the module assume that the PC is not functionning and
perform the reset on PC DC output to restart it.

**Notes on LCD activation**   
This feature will close the LCD if there is no motion detected. In the previous example
the LCD activation is set to 60 minutes and in that case if there is no motion detected
the module will close the LCD after 60 minutes. As soon as a motion is detected, the LCD
DC output will be set to ON and the counter will be reseted to 0 on every motion detected.

**Notes on audio activation**   
Same functionality as the LCD but following a motion detection, it will trigger the sound instead
the LCD.

**HR** Return the day and hour    
String received: H32130   
The second digit is the day. 3 ->  Tuesday   
1 = Sunday, 2 = Monday, 3 = Tuesday .... 7 = Saturday   
The last 4 digits is the hour.  2130 -> 9h30 PM   
Associated command: HW in the second section    

**JR** Return the schedule   
String received: J0100020003000400050014302929    
After the caracter J there are 7 groups of 4 numbers. The first group is the starting hour for the day 1 (Sunday), the
second group is the starting hour for the day 2 (Monday) and so on.    
If a group is 2929 it means the panel won't restart on that associated day   
Based on the received string:    
Sunday the panel will start at 1h00 am   
Monday the panel will start at 2h00 am   
Tuesday the panel will start at 3h00 am   
Wednesday the panel will start at 4h00 am   
Thursday the panel will start at 5h00 am   
Friday the panel will start at 14h30 pm   
Saturday the panel won't start   
Associated command: JW and Qx in the second section    

**TR** Return the motion sensor frequency   
Return a value from 0 to 6   
0 = 1818 Hz  
1 = 2000 Hz   
2 = 2222 Hz   
3 = 2409 Hz   
4 = 2597 Hz   
5 = 2796 Hz   
6 = 2963 Hz   
Associated command Tx in the second section

**UR** Return the motion sensor sensitivity   
Return a value from 0 to 9   
0 = very sensitive and 9 = not sensitive   
Associated command Ux in the second section   



#### 2 -  Commands that modify configuration      
These commands must be followed by a carriage return (enter). If the command is
valid the module will reply OK. If not the module will reply nothing.  

**S2** Activate LCD DC output   
**R2** Desactivate LCD DC output   
**S3** Activate PC DC output   
**R3** Desactivate PC DC output   
**SA** Activate Audio amplifier      
**RA** Desactivate Audio amplifier       

**Ax** Set audio activation    
x = Value from 0 to 9   
A0 = Disable the feature (sound always ON)   
A1 = 1 x 1 = 1 minute   
...   
A9 = 9 x 1 = 9 minutes   

**Lx** Set LCD activation    
x = Value from 0 to 9   
L0 = Disable the feature (LCD always ON)   
L1 = 1 x 10 = 10 minutes   
...   
L9 = 9 x 10 = 90 minutes   

**Dx** Set Watchdog    
x = Value from 0 to 9   
D0 = Disable the watchdog    
D1 = 1 x 5 = 5 minutes   
...   
D9 = 9 x 5 = 45 minutes   

**Fx** Set fan speed    
x = Value from 0 to 9   
F0 = Stops fans    
F1 = Very low speed   
...   
F9 = Very high speed   

**Ux** Set motion detector sensibility    
x = Value from 0 to 9   
See UR value explanation   

**Tx** Set motion detector frequency    
x = Value from 0 to 6   
See TR value explanation   
This feature is to avoid conflicts with other I/R devices   

**Qx** Activate scheduler    
x = Value from 0 to 9   
Q1 = Will set the PC DC output to OFF and activate the scheduler after 1 min   
...   
Q9 = Will set the PC DC output to OFF and activate the scheduler after 9 mins
Q0 = Desactivate de count down    
The delay is necessary to let the PC perform a clean shutdown    
Before use that command, it is very important to have a valid schedule (See JR)
and a valid clock setting (see HR)   

**WD** Save config    
After a configuration change you must save the config if you don't want to loose
your change after a power failure.  

**HW** Set day and hour to the MPact module   
Usage:  HW32130   
The first digit after the HW is the day. 3 ->  Tuesday   
1 = Sunday, 2 = Monday, 3 = Tuesday .... 7 = Saturday   
The last 4 digits is the hour.  2130 -> 9h30 PM   

**JW** Set the schedule   
Usage: JWh1m1h2m2h3m3h4m4h5m5h6m6h7m7    
h1m1 = Hour for day 1 (Sunday)  
...   
h7m7 = Hour for day 7 (Saturday)   
If you want to change only one day, you have to do it for the 7 days
If the command Qx is not activated, the MPact module will not use the scheduler


