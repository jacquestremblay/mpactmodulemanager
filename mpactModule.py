#!/usr/bin/python3
#
# MPact 
# Jacques Tremblay
#
softwareVersion = "1.3.2"

import serial,time,datetime,os
import subprocess

ttySerial = "/dev/ttyUSB0"
globalStatusString = "" 

messageMainMenu = "\nDo you want to return in the main menu ? (Y/N)"

initMessages = (
        "\tSet LCD to ON:\t\t",
        "\tSet CPU to ON:\t\t",
        "\tSet audio to ON:\t",
        "\tSet delay audio:\t",
        "\tDisable WatchDog:\t",
        "\tSet FAN to ON:\t\t",
        "\tDisable delay display:\t",
        "\tSet motion sensor sensibility:",
        "\tSet Frequency:\t\t",
        "\tSet Scheduler:\t\t",
        "\tSave config:\t\t")

frequencyList = ("1818","2000","2222","2409","2597","2796","2963")
sensibilityList = ("100%","90%","80%","70%","60%","50%","40%","30%","20%","10%","0%")
fanSpeedList = ("0%","58%","75%","83%","88%","93%","96%","97%","99%","100%")

dayWeek = ("Monday   ","Tuesday   ","Wednesday","Thursday","Friday   ","Saturday","Sunday   ")
systemDayToMpactDay = {0:2,1:3,2:4,3:5,4:6,5:7,6:1}
mpactDayToSystemDay = {1:6,2:0,3:1,4:2,5:3,6:4,7:5}

# M'Pact init string
cmdInit = ("S2","S3","SA","A3","D0","F1","L0","U0","T0","JW2929292929292929292929292929","WD")

cmdVersion = "VR" # Return MPact module version
cmdStatus = "GS" # Return status string
cmdLcdOn = "S2"  # Set LCD DC output to 12 volts
cmdLcdOff = "R2" # Set LCD DC output to 0 volt
cmdCpuOn = "S3" # Set LCD DC output to 12 volts
cmdCpuOff = "R3" # Set LCD DC output to 0 volt
cmdAudioOn = "SA" # Set audio amplifier to ON
cmdAudioOff = "RA" # Set audio amplifier to  OFF
cmdFanFull = "F9" # Set fan speed to full speed
cmdFanMid = "F2" # Set fan speed to low speed
cmdFanOff = "F0" # Stop fans
cmdFrequency = "T3" # Set frequency detection (value 0 to 6)
cmdFrequencyRead = "TR" # Return frequency 
cmdSensibilityRead = "UR" # Read sensor sensibility
cmdWatchDog5min = "D1" # Set watchdog to 5 min (1 x 5 = 5 min)
cmdWatchDogDisable = "D0" # Disable le watchdog
cmdSaveConfig = "WD" # Save config in the MPact module
carriageReturn = "\r"
cmdReply = "OK" # Return value form a valid command (except GS, UR TR)

# Variable asociated to GS command (Message & Result)
globalStatusDigit = (
        "Digit 1,2\t\t",
        "Digit 3\t\t\t",
        "Digit 4\t\t\t",
        "Digit 5\t\t\t",
        "Digit 6,7,8,9\t\t",
        "Digit 10,11\t\t",
        "Digit 12,13\t\t",
        "Digit 14,15\t\t",
        "Digit 16\t\t",
        "Digit 17\t\t",
        "Digit 18\t\t",
        "Digit 19\t\t",
        "Digit 20\t\t",
        "Digit 21\t\t")

globalStatusMsg = (
        "\tNot Used\t\t",
        "\tLCD DC output\t\t",
        "\tPC DC output\t\t",
        "\tAudio status\t\t",
        "\tConsumption\t\t",
        "\tTemperature\t\t",
        "\tLeft fan speed\t\t",
        "\tRight fan speed\t\t",
        "\tWatch Dog delay\t\t",
        "\tLCD delay\t\t",
        "\tAudio delay\t\t",
        "\tNot used\t\t",
        "\tFan speed set to\t",
        "\tNot used\t\t")
globalStatusResult = [""]*14
globalStatusRaw = [""]*14

mpactModuleMsg = (
        "MPact Version:\t\t\t",
        "Sensor frequency:\t\t",
        "Sensor sensitivity\t\t")
mpactModuleResult = [""]*3

scheduleDigits = (
        "2 to 5\t\t",
        "6 to 9\t\t",
        "10 to 13\t",
        "14 to 17\t",
        "18 to 21\t",
        "22 to 25\t",
        "26 to 29\t")
scheduleRawValue = [""]*7
scheduleUserValue = [""]*7

# TTYport configuration
def serialConfig():
    ser = serial.Serial(ttySerial)
    ser.baudrate = 19200
    ser.bytesize = serial.EIGHTBITS
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.timeout = 0.1 
    ser.xonxoff = False 
    ser.rtscts = False 
    ser.dsrdtr = False 
    ser.writeTimeout = 2
    return ser;

# Wait for enter
def pressEnterKey():
    try:
        print ("\n\tPress \"Enter\" to continue")
        choice = input()
        return
    except KeyboardInterrupt:
        return

# Send command to the MPact module
def sendCommand(cmd):
    ttyPort.flushInput()
    ttyPort.flushOutput()
    ttyPort.write(cmd.encode())
    ttyPort.write(carriageReturn.encode())
    lineReceived = ttyPort.readline().decode()
    lineCleaned = lineReceived.replace("MD","") # get rid of the "MD" message in the result
    lineCleaned = lineCleaned.replace("\r","") # get rid of the cariage return
    if (lineCleaned == ""):
        lineCleaned = "*** No reply received ***"
    return lineCleaned


# Set the time and day on MPact Module
def setMpactModuleClock():
    time  = datetime.datetime.strftime(datetime.datetime.today(),'%H%M')
    dayNumber = datetime.datetime.today().weekday()
    setHourCmd = "HW"+str(systemDayToMpactDay[dayNumber])+time
    sendCommand(setHourCmd)

# Get the time and date form MPact module
def getMpactModuleClock():
    result = sendCommand("HR")
    day = int(result[1:2])
    hour = result[2:4]
    minute = result[4:6]
    weekDays = dayWeek[mpactDayToSystemDay[day]]
    print ("String received from HR command: " + result +" -> " + weekDays.strip() + " " + hour + ":" + minute)

# Get the schedule from MPact module
def getMpactSchedule():
    result = sendCommand("JR")
    y = 1
    z = 5
    for x in range (7):
        scheduleRawValue[x] = result[y:z]
        if (scheduleRawValue[x] == "2929"):
            scheduleUserValue[x] = "No restart"
        else:
            hour = scheduleRawValue[x]
            scheduleUserValue[x] = hour[:2] + ":" + hour[2:]
        y = y+4
        z = z+4

    print ("\nString received from JR command: " + result + "\n")
    printSchedule()

# Print schedule in user format
def printSchedule():
    print ("Digits\t\tValue\tDay\t\tStart time")
    print ("========\t=====\t==========\t==========")
    for x in range (1,8):
        dayName = dayWeek[mpactDayToSystemDay[x]]
        print (scheduleDigits[x-1]+scheduleRawValue[x-1]+"\t" + dayName + "\t" + scheduleUserValue[x-1])

# Fonction who send the init string to the MPact module
#
def initMPactModule():
    print ("\nM'Pact module initializing\n")
    setMpactModuleClock()
    for x in range(len(cmdInit)):
        returnedMessage = sendCommand(cmdInit[x])
        print (initMessages[x] + "\t" + returnedMessage)


# Let a user send a command to the MPact module
def enterCommand():
    os.system('clear')
    inputKey = input("Please type the MPact module command: ")
    receivedReply = sendCommand(inputKey)
    print ("\n***Reply from command " + inputKey + " is: " + receivedReply)
    pressEnterKey()

def listenMpactModule():
    print ("Start listening on " + ttyPort.name + "...<CTRL+c> to stop listening")
    try:
        while True:
            ttyPort.flushInput()
            line = ttyPort.readline().decode().replace("\r"," ")
            print (line,end="", flush=True)
    except KeyboardInterrupt:
        return

# Get the string returned form GS command
#
def getMpactModule():
    global globalStatusString
    globalStatusString = sendCommand(cmdStatus) 
    getNotUsed()
    getLcdStatus()
    getCpuStatus()
    getAudioStatus()
    getAmp()
    getTemp()
    getFanSpeed()
    getWatchDogStatus()
    getLcdDelayStatus()
    getAudioDelayStatus()
    getFanSpeedStatus()
    getVersion()
    getFrequency()
    getSensibility()

# Format not used values in globalStatus string 
def getNotUsed():
    globalStatusRaw[0] = "00"
    globalStatusRaw[11] = "0"
    globalStatusRaw[13] = "0"
    globalStatusResult[0] = "--"
    globalStatusResult[11] = "--"
    globalStatusResult[13] = "--"

def getFrequency():
    frequencyIndex = sendCommand(cmdFrequencyRead)
    frequencyIndex = int(frequencyIndex[2:])
    mpactModuleResult[1] =  str(frequencyList[frequencyIndex] + " Hz (T" + str(frequencyIndex) +")")

def getSensibility():
    sensibility = sendCommand(cmdSensibilityRead)
    index = int(sensibility[2:])
    mpactModuleResult[2] =  sensibilityList[index] + " (U" + str(index) +")"
    
def getWatchDogStatus():
    globalStatusRaw[8] = int(globalStatusString[16:17])
    globalStatusResult[8] =  str(globalStatusRaw[8] * 5)+" minute(s)"

def getLcdDelayStatus():
    globalStatusRaw[9] = int(globalStatusString[17:18])
    globalStatusResult[9] = str(globalStatusRaw[9] * 10) + " minute(s)"

def getAudioDelayStatus():
    globalStatusRaw[10] = int(globalStatusString[18:19])
    globalStatusResult[10] = str(globalStatusRaw[10]) + " minute(s)"

def getFanSpeedStatus():
    globalStatusRaw[12] = str(globalStatusString[20:21])
    globalStatusResult[12] = fanSpeedList[int(globalStatusRaw[12])]

def getLcdStatus():
    globalStatusRaw[1] = globalStatusString[3:4]
    if (globalStatusRaw[1]) == '0':
        globalStatusResult[1] = "LCD OFF";
    else:
        globalStatusResult[1] = "LCD ON";

def getCpuStatus():
    globalStatusRaw[2] = globalStatusString[4:5]
    if (globalStatusRaw[2]) == '0':
        globalStatusResult[2] = "PC OFF";
    else:
        globalStatusResult[2] =  "PC ON";

def getVersion():
    mpactModuleResult[0] =  sendCommand(cmdVersion)

def getAudioStatus():
    globalStatusRaw[3] = globalStatusString[5:6]
    if (globalStatusRaw[3]) == '0':
        globalStatusResult[3] = "Sound OFF";
    else:
        globalStatusResult[3] = "Sound ON";

def getAmp():
    globalStatusRaw[4] = globalStatusString[6:10]
    globalStatusResult[4] =  str(globalStatusRaw[4]) + " ma"

def getTemp():
    globalStatusRaw[5] =  str(globalStatusString[10:12])
    globalStatusResult[5] = globalStatusRaw[5] + " Celsius"

def getFanSpeed():
    globalStatusRaw[6] = str(globalStatusString[14:16])
    globalStatusRaw[7] = str(globalStatusString[12:14])
    globalStatusResult[6] = str((int(globalStatusRaw[6]))*100) + " RPM"
    globalStatusResult[7] = str((int(globalStatusRaw[7]))*100) + " RPM"

def displaySysInfo():

    print ("\nString received from GS command: " + globalStatusString + "\n")
    print ("Digits\t\t      Value\tValue name\t\tResult")
    print ("============\t     =======\t=============\t\t============")
    for x in range(len(globalStatusMsg)):
        print (globalStatusDigit[x] + str(globalStatusRaw[x]) + globalStatusMsg[x] + str(globalStatusResult[x]))
    
    print ("\nOther values")
    print ("=======================================================")
    for x in range(len(mpactModuleMsg)):
        print (mpactModuleMsg[x] + str(mpactModuleResult[x]))

    print ("\nScheduler")
    print ("=======================================================")
    getMpactModuleClock()
    getMpactSchedule()

def mainMenu():
   
    os.system('clear')
    print ("\nM'Pact module manager  version " + softwareVersion + "  Press <CTRL+c> to exit" + "\n")
    print ("\t1 - MPact module initialisation")
    print ("\t2 - Display MPact module config")
    print ("\t3 - Send command to MPact module")
    print ("\t4 - Listen to MPact module")
    choice = input().upper()
    if (choice == "1"):
        os.system('clear')
        initMPactModule()
        pressEnterKey()
        mainMenu()
    elif (choice == "2"):
        os.system('clear')
        getMpactModule()
        displaySysInfo()
        pressEnterKey()
        mainMenu()
    elif (choice == "3"):
        enterCommand() 
        mainMenu()
    elif (choice == "4"):
        listenMpactModule() 
        mainMenu()
    else:
        mainMenu()


############################################
# Main program
#

try:
    try:
        ttyPort=serialConfig()        
        
        while ttyPort.isOpen():
            if (sendCommand(carriageReturn) != cmdReply):
                print ("TTY port doesn't respond... reset TTY port")
                time.sleep(.5)
                ttyPort.close()
                ttyPort=serialConfig()
            else:
                mainMenu()
    
    except serial.serialutil.SerialException:
        print ("Can't open serial port")
        time.sleep(1)

except KeyboardInterrupt:
    print("....Quit program")
    ttyPort.close()

#Last line
