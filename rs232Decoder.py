#!/usr/bin/python3
#
# MPact 
# Jacques Tremblay
#
softwareVersion = "1.3.2"

import serial,time,datetime,os
import subprocess

ttySerial = "/dev/ttyUSB1"
openString = "\x55\x56\x00\x00\x00\x01\x01\xad" 

# TTYport configuration
def serialConfig():
    ser = serial.Serial(ttySerial)
    ser.baudrate = 9600
    ser.bytesize = serial.EIGHTBITS
    ser.parity = serial.PARITY_NONE
    ser.stopbits = serial.STOPBITS_ONE
    ser.timeout = 0.1 
    ser.xonxoff = False 
    ser.rtscts = False 
    ser.dsrdtr = False 
    ser.writeTimeout = 2
    return ser;


# Send command 
def sendCommand():
#    ttyPort.flushInput()

    ttyPort.write(serial.to_bytes([0x55,0x56,0x00,0x00,0x00,0x01,0x01,0xad]))
    time.sleep(2) 
    ttyPort.write(serial.to_bytes([0x55,0x56,0x00,0x00,0x00,0x01,0x02,0xae]))
    ttyPort.flushOutput()

def listenMpactModule():
    print ("Start listening on " + ttyPort.name + "...<CTRL+c> to stop listening")
    try:
        while True:
        #    ttyPort.flushInput()
            line = ttyPort.read().hex()
            print (line,end="", flush=True)
    except KeyboardInterrupt:
        return

############################################
# Main program
#

ttyPort=serialConfig()        
listenMpactModule()
#sendCommand()
ttyPort.close()

#Last line
