#!/usr/bin/python3
#
# MPact 
# Jacques Tremblay
#
# Programme qui envoie des codes hexa via une connections TCP vers un mikrotik 
# dont le port console est configuré en remote-access.
# Ceci permet de contrôler le module relais R223B01
# Le programme envois des strings de codes hexadécimales au R223B01

softwareVersion = "1.1"

import os,sys,socket,time,errno

port = 10000

# String de codes hexa pour contrôler le R223B01
readStatusHexCode = b'\x55\x56\x00\x00\x00\x01\x00\xac' # Lire l'état du relais
toggleHexCode = b'\x55\x56\x00\x00\x00\x01\x03\xaf' # Bascule le relais dans son état inverse 
pcOffHexCode = b'\x55\x56\x00\x00\x00\x01\x01\xad' # Ouvre le relais
pcOnHexCode =  b'\x55\x56\x00\x00\x00\x01\x02\xae' # Ferme le relais
reponseOffHexCode =  b'\x33\x3c\x00\x00\x00\x01\x01\x71' # Réponse reçue lorsque relais est ouvert
reponseOnHexCode =   b'\x33\x3c\x00\x00\x00\x01\x02\x72' # Réponse reçue lorsque relais est fermé 

optionTable = ("--restart","--toggle","--status","--on","--off")

# Fonction envois la commande reçue en paramêtre et l'envois au module relais
def sendCommand(cmdToSend):
    try:
        tcpClient.send(cmdToSend)
        tcpClient.settimeout(5.0)
        dataReceived = tcpClient.recv(1024)
        return dataReceived
    except socket.timeout:
        print ("Module relais ne répond pas")
        quit()

# Fonction qui  valide le format de l'adresse IP
def validateIpAdress(addr):
    parts = addr.split(".")
    if (len(parts) != 4):
        print ("Addresse IP invalide")
        return False
    for item in parts:
        if not 0 <= int(item) <= 255:
            print ("Addresse IP invalide")
            return False
    return True

# Fonction qui ouvre le relais pour couper l'alimentation du PC
def powerOff():
    print ("On ferme l'ordinateur")
    status = sendCommand(readStatusHexCode)
    if (status == reponseOnHexCode):
        sendCommand(pcOffHexCode)
        printStatus()
    elif (status == reponseOffHexCode):
        print ("Déjà à OFF")

# Fonction qui ferme le relais pour alimenter du PC
def powerOn():
    print ("On allume l'ordinateur")
    status = sendCommand(readStatusHexCode)
    if (status == reponseOffHexCode):
        sendCommand(pcOnHexCode)
        printStatus()
    elif (status == reponseOnHexCode):
        print ("Déjà à ON")

# Fonction qui éteint le PC attend 5 secondes et le réalimente
def cyclePower():
    powerOff()
    print ("On attend 5 secondes")
    time.sleep(5) 
    powerOn()

# Fonction qui change l'état du relais à son opposé
def togglePower():
    print ("État actuel:\t", end="")
    printStatus()
    sendCommand(toggleHexCode)
    print ("Maintenant:\t", end="")
    printStatus()

# Fonction qui affiche l'état du relais
def printStatus():
    reply = sendCommand(readStatusHexCode)
    if (reply == reponseOffHexCode):
        print ("Le PC est OFF")
    elif (reply == reponseOnHexCode):
        print ("Le PC est ON")
    else:
        print ("Reponse inconnue")

# Fonction qui vérifie si le relais répond
def checkRelayModule():
    reply = sendCommand(readStatusHexCode)
    if (reply == reponseOffHexCode or reply == reponseOnHexCode):
        return True
    else:
        return False

# Fonction qui vérifie la validité de la commande
# Valide que le premier argument reçu soit l'adresse IP
# Ensuite vérifie que le deuxième argument soit une option valide
# Retourne un tableau comprenant les deux arguments validés
# Le premier élément du tableau est l'adresse IP et le deuxième est l'option
# Sinon imprime comment utiliser la commande et sort du programme
def checkArgument():
    argTable = ["",""] #Définit le tableau des options à retourner
    if (len(sys.argv) == 3): #On valide qu'on a bien 3 arguments (la commande compte pour un)
        if (validateIpAdress(str(sys.argv[1]))):
            argTable[0] = (str(sys.argv[1]))
            argTable[1] = str(sys.argv[2])
            # Vérifie que l'option reçu soit valide
            for item in optionTable:
                if (item == argTable[1]):
                    return argTable
    printHelp()
    quit()
        
# Fontion qui imprime comment utiliser la commande
def printHelp():
    print (sys.argv[0] + " ipddress options")
    print (" Les options sont:")
    print ("   --restart\tÉteint le PC, attend 5 sec et repart le PC")
    print ("   --on\t\tAllume le PC")
    print ("   --off\tÉteint le PC")
    print ("   --toggle\tChange l'état du PC")
    print ("   --status\tAffiche l'état du PC")


############################################
# Programme principal
#

returnedArguments = checkArgument()
ipAdress = returnedArguments[0]
option = returnedArguments[1]

# On monte la connection TCP vers le mikrotik
# Si pas 
try:
    try:
        tcpClient = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcpClient.connect((ipAdress, port))
        print ("Connection TCP au Mikrotik OK")
    except socket.error as error:
        print ("Erreur de connection au Mikrotik: ", end="")
        print (os.strerror(error.errno))
        quit()

    if (checkRelayModule()):
        if (option == "--restart"):
            cyclePower()
        elif (option == "--on"):
            powerOn()
        elif (option == "--off"):
            powerOff()
        elif (option == "--toggle"):
            togglePower()
        elif (option == "--status"):
            printStatus()
    else:
        print ("Le module relais ne répond pas")

except KeyboardInterrupt:
    print("...Interruption de la commande")

tcpClient.close()

#Last line
